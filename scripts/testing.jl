using CircuitSim
using Logging
# https://asciiflow.com/#/

#############################################
#=
    Thevenin voltage + resistor
=#
#############################################
global_logger(Logging.ConsoleLogger(Logging.Warn))
global_logger(Logging.ConsoleLogger(Logging.Debug))

n0 = Node(:n0)
n1 = Node(:n1)
r1 = Resistor(4.0, n1, n0)
vS = VoltageSource(1.0, n1, n0)

c = Circuit([r1, vS])

solve!(c)

for i in 1:length(eachrow(c.A))
    println(inspect_system(c, i))
end
# V_1 - V_2 = 4 * I_1      Ohm's Law
# V_1 - V_2 = 1            Voltage Source
# V_1 = 0                  Fixing a reference voltage
# I_1 + I_2 = 0            KCL at one node

voltage_across(c, vS)
voltage_across(c, r1)
current_through(c, vS)
current_through(c, r1)

#############################################
#=
    Norton current + resistor
=#
#############################################
global_logger(Logging.ConsoleLogger(Logging.Warn))

n0 = Node(:n0)
n1 = Node(:n1)
r1 = Resistor(4.0, n1, n0)
iS = CurrentSource(3.0, n0, n1)

c = Circuit([r1, iS])

solve!(c)

voltage_across(c, r1)
voltage_across(c, iS)
current_through(c, r1)
current_through(c, iS)

#############################################
#=
    Parallel resistors with voltage source
=#
#############################################
global_logger(Logging.ConsoleLogger(Logging.Warn))

n0 = Node(:n0)
n1 = Node(:n1)
r1 = Resistor(4.0, n1, n0)
r2 = Resistor(8.0, n1, n0)
vS = VoltageSource(1.0, n1, n0)

c = Circuit([r1, r2, vS])

solve!(c)

@test voltage_across(c, vS) ≈ 1
@test voltage_across(c, r1) ≈ 1
@test voltage_across(c, r2) ≈ 1
@test current_through(c, vS) ≈ -0.375
@test current_through(c, r1) ≈ 0.25
@test current_through(c, r2) ≈ 0.125

#############################################
#=
    Parallel at resonance, 1/sqrt(LC) == ω
=#
#############################################
global_logger(Logging.ConsoleLogger(Logging.Warn))

n0 = Node(:n0)
n1 = Node(:n1)
l1 = Inductor(.25, n1, n0)
c1 = Capacitor(4, n1, n0)
v1 = VoltageSource(1.0, n1, n0)

c = Circuit([v1, l1, c1])

solve!(c)

current_through(c, v1)
current_through(c, l1)
current_through(c, c1)

#############################################
#=
    Series at resonance, 1/sqrt(LC) == ω
=#
#############################################
global_logger(Logging.ConsoleLogger(Logging.Warn))

n0 = Node(:n0)
n1 = Node(:n1)
n2 = Node(:n2)
l1 = Inductor(0.25, n1, n2)
c1 = Capacitor(4, n2, n0)
i1 = CurrentSource(1.0, n0, n1)

c = Circuit([l1, c1, i1])

solve!(c)

voltage_across(c, i1)
voltage_across(c, l1)
voltage_across(c, c1)

