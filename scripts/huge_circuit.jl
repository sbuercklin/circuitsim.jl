using CircuitSim
using BenchmarkTools
using ProfileView

function simulate_iterations(iterations)
    n0 = Node(:n_0_0)
    n1 = Node(:n_0_1)

    vS = VoltageSource(1.0, n1, n0)

    elements = CircuitSim.TwoTerminalDevice[vS];

    for k in 1:iterations
        nkm_0 = Node(Symbol("n_$(k-1)_0"))
        nkm_1 = Node(Symbol("n_$(k-1)_1"))
        nk_0 = Node(Symbol("n_$(k)_0"))
        nk_1 = Node(Symbol("n_$(k)_1"))

        r1_k = Resistor(1.0, nkm_1, nk_1)
        r2_k = Resistor(1.0, nk_1, nk_0)
        r3_k = Resistor(1.0, nk_0, nkm_0)

        push!(elements, r1_k, r2_k, r3_k)
    end

    c = Circuit(elements);

    solve!(c);

    return current_through(c, vS)
end

@btime simulate_iterations(200)
@btime simulate_iterations(400)
@btime simulate_iterations(600)
@btime simulate_iterations(1200)

@profview simulate_iterations(200)
@profview simulate_iterations(400)
@profview simulate_iterations(800)
@profview simulate_iterations(1600)