using CircuitSim
using Test

#############################################
#=
    Voltage + resistor
=#
#############################################

@testset "Voltage + resistor" begin
    n0 = Node(:n0)
    n1 = Node(:n1)
    r1 = Resistor(4.0, n1, n0)
    vS = VoltageSource(1.0, n1, n0)

    c = Circuit([r1, vS])

    solve!(c)

    @test voltage_across(c, vS) ≈ 1
    @test voltage_across(c, r1) ≈ 1
    @test current_through(c, vS) ≈ -0.25
    @test current_through(c, r1) ≈ 0.25
end

@testset "Current + resistor" begin
    n0 = Node(:n0)
    n1 = Node(:n1)
    r1 = Resistor(4.0, n1, n0)
    iS = CurrentSource(3.0, n0, n1)

    c = Circuit([r1, iS])

    solve!(c)

    @test voltage_across(c, r1) ≈ 12
    @test voltage_across(c, iS) ≈ -12
    @test current_through(c, r1) ≈ 3.0
    @test current_through(c, iS) ≈ 3.0
end

@testset "Voltage + parallel resistors" begin
    n0 = Node(:n0)
    n1 = Node(:n1)
    r1 = Resistor(4.0, n1, n0)
    r2 = Resistor(8.0, n1, n0)
    vS = VoltageSource(1.0, n1, n0)

    c = Circuit([r1, r2, vS])

    solve!(c)

    @test voltage_across(c, vS) ≈ 1
    @test voltage_across(c, r1) ≈ 1
    @test voltage_across(c, r2) ≈ 1
    @test current_through(c, vS) ≈ -0.375
    @test current_through(c, r1) ≈ 0.25
    @test current_through(c, r2) ≈ 0.125
end

@testset "Series resonance, current source" begin
    n0 = Node(:n0)
    n1 = Node(:n1)
    n2 = Node(:n2)
    l1 = Inductor(0.25, n1, n2)
    c1 = Capacitor(4, n2, n0)
    i1 = CurrentSource(1.0, n0, n1)

    c = Circuit([l1, c1, i1])

    solve!(c)

    @test voltage_across(c, i1) == 0.0
    @test voltage_across(c, l1) ≈ 0.25*im
    @test voltage_across(c, c1) ≈ -0.25*im
    @test current_through(c, i1) ≈ 1.0
    @test current_through(c, l1) ≈ 1.0
    @test current_through(c, c1) ≈ 1.0
end