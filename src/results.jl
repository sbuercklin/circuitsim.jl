function voltage_across(c, e)
    plus, minus = plus_minus_idxs(c, e)
    return c.x[plus] - c.x[minus]
end

function voltage_across(c, n1, n2)
    v1_idx = voltage_node_index(c, n1)
    v2_idx = voltage_node_index(c, n2)

    return c.x[v1_idx] - c.x[v2_idx]
end

function current_through(c, e)
    idx = branch_index(c, e)
    return c.x[idx]
end