function solve!(c::Circuit)
    # Assume we have a nontrivial solution; if there's no source, 
    #   then the only solution is the zero vector
    if !any(e -> isa(e, VoltageSource) || isa(e, CurrentSource), c.elements) 
        return zero(c.b) 
    end
    nnodes = length(c.nodes)
    nels = length(c.elements)
    
    problem_size = nnodes + nels

    # !!!!!!!!!!!!! nnodes vs nels?
    c.A[nels+1,1] = 1 # First node always has a voltage of 0

    # Use the constitutive relation for each element
    for el in c.elements
        constitutive_relation!(c, el)
    end

    # Use KCL at every node, except first one
    for idx in 2:length(c.nodes)
        n = c.nodes[idx]
        KCL!(c, n)
    end

    c.x .= c.A \ c.b
    return c.x
end

# The default case for R/L/C
# This is just Ohm's law enforced as Vi - Vj = Iij * Zij
function constitutive_relation!(c, e::TwoTerminalDevice)
    @debug "Adding equation for a $(typeof(e)) with impedance $(impedance(e))"
    row = el_idx(c, e)
    plus, minus = plus_minus_idxs(c, e)
    br_idx = branch_index(c, e)
    c.A[row, plus] = 1
    c.A[row, minus] = -1

    c.A[row, br_idx] = -impedance(e)
    # @infiltrate
end

# For a voltage source, we set Vi - Vj = V where V is given
function constitutive_relation!(c, e::VoltageSource)
    @debug "Adding constraint for a VoltageSource with voltage $(e.V)"
    row = el_idx(c, e)
    plus, minus = plus_minus_idxs(c, e)

    c.A[row, plus] = 1
    c.A[row, minus] = -1
    c.b[row] = e.V
end

# For a current source, we set Iij = I wher I is given
function constitutive_relation!(c, e::CurrentSource)
    row = el_idx(c, e)
    col = branch_index(c, e)

    c.A[row, col] = 1
    c.b[row] = e.I
end

function KCL!(c, n::Node)
    @debug "Doing KCL at $n"
    row = KCL_node_index(c, n)
    for el in c.elements
        if el.plus == n
            col = branch_index(c, el)
            c.A[row, col] = 1 # outgoing currents
        elseif el.minus == n
            col = branch_index(c, el)
            c.A[row, col] = -1 # ingoing current
        end
    end
end