struct Circuit{TN, TE, TA, TB, TX}
    nodes::TN
    first::Node
    elements::TE
    A::TA
    x::TX
    b::TB
end

function Circuit(elements)
    nodes = OrderedSet(Node[]) # Remove for the debugger
    for e in elements
        push!(nodes, e.plus)
        push!(nodes, e.minus)
    end
    firstnode = first(nodes)

    problem_size = length(nodes) + length(elements)
    # Turn this into a linear system, Ax = b, where A comes from KCL and node voltages
    #   and b is the heterogeneous term arising from sources
    # The first `nnodes` entries will be our node voltages
    # the last `nels` entries will be our element currents
    matrix = zeros(ComplexF64, (problem_size, problem_size)) # A
    solution = zeros(ComplexF64, problem_size) # x
    constant_term = deepcopy(solution) # b

    return Circuit(nodes, firstnode, elements, matrix, solution, constant_term)
end

# Find which row corresponds to a given element constitutive relation
el_idx(c, el) = findfirst(isequal(el), c.elements) 

# Find the solution entry corresponding to a given node voltage
voltage_node_index(c, node) = findfirst(isequal(node), c.nodes)

# Find which row corresponds to a given KCL node
KCL_node_index(c, node) = findfirst(isequal(node), c.nodes) + length(c.elements) 

# Gets the node voltage indices for the Circuit matrix
function plus_minus_idxs(c, elt)
    plus_idx = voltage_node_index(c, elt.plus)
    minus_idx = voltage_node_index(c, elt.minus)
    return (plus_idx, minus_idx)
end

# Gets the branch current index for the Circuit matrix
function branch_index(c, elt)
    offset = length(c.nodes)
    return offset + findfirst(isequal(elt), c.elements) 
end

function find_starting_elts(c, n)
    return filter(elt -> elt.plus == n, c.elements)
end
function find_ending_elts(c, n)
    return filter(elt -> elt.minus == n, c.elements)
end