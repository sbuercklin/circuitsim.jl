module CircuitSim

using Logging

using OrderedCollections

using Infiltrator

const j = 1.0im

include("./Node.jl")
export Node

include("./Elements.jl")
export Resistor, Capacitor, Inductor, VoltageSource, CurrentSource

include("Circuit.jl")
export Circuit

include("./solve.jl")
export solve!

include("./results.jl")
export voltage_across, current_through

include("./utils.jl")
export inspect_system

end # module
