function inspect_system(c, row)
    A_row = c.A[row,:]
    b_entry = c.b[row]

    n_nodes = length(c.nodes)
    n_els = length(c.elements)

    row_str = ""
    for (idx,entry) in enumerate(A_row)
        if !iszero(entry)
            coeff = idx <= n_els ? "V_$idx" : "I_$(idx - n_els)"
            coeff *= ")"
            if entry == -1
                coeff = "-" * coeff
            elseif !isone(entry)
                coeff = "($entry) * " * coeff
            end
            coeff = "(" * coeff

            row_str *= coeff
        end
        if idx < length(A_row) && any(e -> !iszero(e), A_row[(idx+1):end]) && row_str != ""
            row_str *= " + "
        end
    end

    row_str *= " = $b_entry"

    return row_str
end