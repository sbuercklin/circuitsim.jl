abstract type TwoTerminalDevice end

abstract type ReactiveResistiveDevice <: TwoTerminalDevice end

struct Resistor{TR} <: ReactiveResistiveDevice
    z::TR
    plus::Node
    minus::Node
    function Resistor(R, plus, minus; ω = 1.0)
        new{typeof(R)}(R, plus, minus)
    end
end

struct Capacitor{TC} <: ReactiveResistiveDevice
    z::TC
    plus::Node
    minus::Node
    function Capacitor(C, plus, minus; ω = 1.0)
        z = 1/(j * ω * C)
        new{typeof(z)}(z, plus, minus)
    end
end

struct Inductor{TL} <: ReactiveResistiveDevice
    z::TL
    plus::Node
    minus::Node
    function Inductor(L, plus, minus; ω = 1.0)
        z = j * ω * L
        new{typeof(z)}(z, plus, minus)
    end
end

struct VoltageSource{T} <: TwoTerminalDevice
    V::T
    plus::Node
    minus::Node
end

struct CurrentSource{T} <: TwoTerminalDevice
    I::T
    plus::Node
    minus::Node
end

impedance(el::ReactiveResistiveDevice) = el.z
impedance(el::VoltageSource) = 0.0
impedance(el::CurrentSource) = Inf 
